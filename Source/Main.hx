package;

import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;

import map.TileMap;

class Main extends Sprite {

	public function new() {
		super();
		
		var map = new TileMap();
		addChild(map);
		
	}
	
	public static function main() {
		Lib.current.addChild(new Main());
	}
}
