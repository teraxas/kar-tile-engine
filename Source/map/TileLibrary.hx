package map;

import flash.display.Sprite;
import flash.display.Bitmap;
import openfl.Assets;
import haxe.ds.IntMap;

import map.tile.Tile;

/*
	Loads tiles on demand
*/
class TileLibrary {

	private static var instance:TileLibrary;
	
	private var filepaths:Map<Int,String>;
	private var tiles:Map<Int,Bitmap>;

	public static inline function getInstance() {
    	if (instance == null)
          return instance = new TileLibrary();
      	else
          return instance;
  	}

	public function new() {
		clearAll();
		
		// TODO: think of something better - dynamical
		filepaths.set(1, "assets/grassLeftBlock.png");
		filepaths.set(2, "assets/grassCenterBlock.png");
		filepaths.set(3, "assets/grassRightBlock.png");
		filepaths.set(4, "assets/goldBlock.png");
		filepaths.set(5, "assets/globe.png");
		filepaths.set(6, "assets/mushroom.png");
		
	}

	// FIXME : evil error
	public function getTile(id:Int):Bitmap {
		if (tiles.exists(id)) {
			return tiles.get(id);
		}
		if (filepaths.exists(id)) {
			tiles.set(id, new Bitmap(Assets.getBitmapData(filepaths.get(id))));
			return tiles.get(id);
		}
		return Null<Bitmap>;
	}

	public function clearAll() {
		filepaths = new IntMap<String>();
		tiles = new IntMap<Bitmap>();
	}

}