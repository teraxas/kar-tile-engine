package map;

import flash.display.Sprite;
import flash.display.Bitmap;
import openfl.Assets;

import map.TileLibrary;
import map.tile.Tile;

// Map class
// TODO : camera movement movement
// TODO : modify sizes
// TODO : optional background
class TileMap extends Sprite {
	
	public static var TILE_WIDTH = 60;
	public static var TILE_HEIGHT = 60;
	public static var SCREEN_WIDTH = 600;
	public static var SCREEN_HEIGHT = 360;

	private var map:Array<Int>;
	private var background:Bitmap;

	public function new() {
		super();
		
		// instantiate new map
		// TODO : load from assets
		map = new Array<Int>();
		map = [	0, 4, 0, 0, 0, 0, 0, 5, 0, 0, 
				0, 0, 0, 0, 0, 1, 2, 2, 3, 0, 
				0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 
				1, 2, 2, 3, 0, 0, 0, 0, 0, 0, 
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
				0, 0, 0, 0, 0, 0, 1, 2, 2, 3];
				
		// load background
		background = new Bitmap(Assets.getBitmapData("assets/background.png"));
		addChild(background);

		// use w and h for map dimensions
		var w = Std.int(SCREEN_WIDTH / TILE_WIDTH);
		var h = Std.int(SCREEN_HEIGHT / TILE_HEIGHT);
		
		for (i in 0...map.length) {
			var tile = new Tile(TileLibrary.getInstance().getTile(map[i]));
			var x = i % w;
			var y = Math.floor(i / w);
			tile.setLoc(x, y);
			addChild(tile);
		}
	}
	
}