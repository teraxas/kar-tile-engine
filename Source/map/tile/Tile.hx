package map.tile;

import flash.display.Sprite;
import flash.display.Bitmap;
import openfl.Assets;

import map.TileMap;

class Tile extends Sprite {
	
	private var image:Bitmap;

	public function new(image:Bitmap) {
		super();
		
		if(image != null) addChild(image);
	}
	
	public function setLoc(x:Int, y:Int) {
		if (image != null) {
			image.x = x * TileMap.TILE_WIDTH;
			image.y = y * TileMap.TILE_HEIGHT;	
		}
	}
}